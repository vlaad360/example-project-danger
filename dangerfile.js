const { schedule } = require("danger");
const { yarnAudit } = require("danger-plugin-audit");

// Note: You need to use schedule() check
schedule(yarnAudit());
